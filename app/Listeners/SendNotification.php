<?php

namespace App\Listeners;

use App\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        $msg = $event->msg;
        
        Mail::send('email.contact_message_notification', ['msg' => $msg], function($m) use ($msg) {
            $m->from('info@blog.com', 'Info');
            $m->to('admin@blog.com', 'Laravel Blog Admin');
            $m->subject(sprintf('A new contact message was received from %s', $msg->sender));
        });
    }
}
