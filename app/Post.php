<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function categories()
    {
    	return $this->belongsToMany('App\Category', 'posts_categories');
    }
    
    public function getRules()
    {
    	$rules = [
            'title' => 'required|max:120',
            'author' => 'required|max:80',
            'body' => 'required'
        ];
    	
    	return $rules;
    }
}
