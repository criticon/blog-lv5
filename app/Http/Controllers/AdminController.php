<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller 
{
    public function getIndex() 
    {
        $posts = Post::orderBy('created_at', 'desc')->take(3)->paginate(5);
        
        return view('admin.index', ['posts' => $posts]);
    }
    
    public function getLogin()
    {
        return view('admin.login');
    }
    
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if (!Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return redirect()->back()->with(['fail' => 'Could not log you in']);
        }
        
        return redirect()->route('admin.index');
    }
    
    public function getLogout()
    {
        Auth::logout();
        
        return redirect()->route('blog.index');
    }
}
