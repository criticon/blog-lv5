<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ContactMessage;
use Illuminate\Support\Facades\Event;
use App\Events\MessageSent;

class ContactMessageController extends Controller
{
    public function getContactIndex()
    {
        return view('frontend.other.contact');
    }
    
    public function postSendMessage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'email' => 'required|email',
            'subject' => 'required|max:140',
            'message' => 'required|min:10',
        ]);
        
        $msg = new ContactMessage();
        $msg->sender = $request['name'];
        $msg->email = $request['email'];
        $msg->subject = $request['subject'];
        $msg->body = $request['message'];
        $msg->save();
        
        Event::fire(new MessageSent($msg));
        
        return redirect()->route('contact')->with(['success' => 'Message successfully sent']);
    }
}
