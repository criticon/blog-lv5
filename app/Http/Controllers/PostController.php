<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

class PostController extends Controller
{
    public function getBlogIndex()
    {
        $posts = Post::paginate(5);
        
        foreach ($posts as $post) {
            $post->body = $this->shortenText($post->body, 50);
        }
        
        return view('frontend.blog.index', ['posts' => $posts]);
    }
    
    public function getPostSingle($post_id, $end = 'frontend')
    {
        $post = Post::find($post_id);
        
        if (!$post) {
            return redirect()->route('blog.index')->with(['faiil' => 'Post not found']);
        }
        
        return view($end . '.blog.single', ['post' => $post]);
    }
    
    public function getUpdatePost($post_id)
    {
        $post = Post::find($post_id);
        
        if (!$post) {
            return redirect()->route('blog.index')->with(['faiil' => 'Post not found']);
        }
        
        return view('admin.blog.edit_post', ['post' => $post]);
    }
    
    public function postUpdatePost(Request $request)
    {
        $this->validate($request, (new Post)->getRules());
        
        $post = Post::find($request['post_id']);
        $post->title = $request['title'];
        $post->author = $request['author'];
        $post->body = $request['body'];
        $post->update(); //TODO check succseed
        
        return redirect()->route('admin.index')->with(['success' => 'Post successfully updated']);
    }
    
    public function getDeletePost($post_id)
    {
        $post = Post::find($post_id);
        
        if (!$post) {
            return redirect()->route('blog.index')->with(['faiil' => 'Post not found']);
        }
        $post->delete();
        
        return redirect()->route('admin.index')->with(['success' => 'Post successfully deleted']);
    }
    
    public function getCreatePost()
    {
        return view('admin.blog.create_post');
    }
    
    public function postCreatePost(Request $request)
    {
        $this->validate($request, (new Post)->getRules());
        
        $post = new Post();
        $post->title = $request['title'];
        $post->author = $request['author'];
        $post->body = $request['body'];
        $post->save();
        
        return redirect()->route('admin.index')->with(['success' => 'Post succesfully created']);
    }
    
    private function shortenText($text, $cntr)
    {
        if (str_word_count($text, 0) > $cntr) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$cntr]) . '...';
        }
        
        return $text;
    }
}
