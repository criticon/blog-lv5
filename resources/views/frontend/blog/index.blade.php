@extends('layouts.master')

@section('title', 'Blog Index')

@section('content')
    @include('includes.info-box')
    
    @foreach($posts as $post)
        <article>
            <h3>{{ $post->title }}</h3>
            <span>{{ $post->author }} | {{ $post->created_at }}</span>
            <p>{{ $post->body }}</p>
            <a href="{{ route('blog.single', ['post_id' => $post->id, 'end' => 'frontend']) }}">Read more...</a>
        </article>
    @endforeach
    @if($posts->lastPage() > 1)
        <ul class="pagination">
        	{{ $posts->links() }}
        </ul>
    @endif
@endsection