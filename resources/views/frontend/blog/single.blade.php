@extends('layouts.master')

@section('title', $post->title)

@section('content')

    <h3>{{ $post->title }}</h3>
    <span>{{ $post->author }} | {{ $post->created_at }}</span>
    <p>{{ $post->body }}</p>

@endsection