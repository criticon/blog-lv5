@extends('layouts.master')

@section('title', 'Contact')

@section('content')

    @include('includes.info-box')
    
    <form action="{{ route('contact.send') }}" method="post">
        <div class="form-group">
            <label for="name">Your Name *</label>
            <input type="text" name="name"  id="name" placeholder="Name" value="{{ Request::old('name') }}" class="form-control" />
        </div>
        <div class="form-group">
            <label for="email">Your Email *</label>
            <input type="text" name="email"  id="email" placeholder="Email" value="{{ Request::old('email') }}" class="form-control" />
        </div>
        <div class="form-group">
            <label for="subject">Subject *</label>
            <input type="text" name="subject"  id="subject" placeholder="Subject" value="{{ Request::old('subject') }}" class="form-control" />
        </div>
        <div class="form-group">
            <label for="message">Message *</label>
            <textarea name="message" id="message" rows="10" placeholder="Message" class="form-control">{{ Request::old('message') }}</textarea>
        </div>
        <button type="submit" class="btn">Submit Message</button>
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
    </form>

@endsection