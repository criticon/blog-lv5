
@if(Session::has('fail'))
    <section>
        {{ Session::get('fail') }}
    </section>
@endif

@if(Session::has('success'))
    <section>
        {{ Session::get('success') }}
    </section>
@endif

@if(count($errors) > 0)
    <div class="col-md-6 col-md-offset-3 text-center">
        <ul class="list-group">
        @foreach($errors->all() as $error)
            <li class="list-group-item">{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    <div class="clear-fix"></div>
@endif

