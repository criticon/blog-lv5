
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <header>
            	<nav class="navbar navbar-default">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse">
                		<ul class="nav navbar-nav">
                			<li><a href="{{ route('about') }}">About</a></li>
                		</ul>
                    </div>
            	</nav>
            </header>
        </div>
    </div>