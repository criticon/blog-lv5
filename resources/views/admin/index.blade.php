@extends('layouts.admin-master')

@section('content')

    @include('includes.info-box')
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="{{ route('admin.blog.create_post') }}">New Post</a>
        </div>
        <div class="panel-body">
        @if(count($posts) === 0)
            No Posts
        @else
            @foreach($posts as $post)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>
                       <h3>{{ $post->title }}</h3>
                       <span>{{ $post->author }} | {{ $post->created_at }}</span>
                    </div>
                </div>
                <div class="panel-footer">
                    <span><a href="{{ route('admin.blog.post', ['post_id' => $post->id, 'end' => 'admin']) }}">View Post</a></span>
                    <span><a href="{{ route('admin.blog.post.edit', ['post_id' => $post->id]) }}">Edit</a></span>
                    <span><a href="{{ route('admin.blog.post.delete', ['post_id' => $post->id]) }}">Delete</a></span>
                </div>
            </div>
            @endforeach
        @endif
        </div>
    </div>
    
    <!-- 
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul>
                <li href="#">Show All Messages</li>
        </div>
        <div class="panel-body">
            <ul>
                <li>No Messages</li>
                <li>
                   <div data-message="message body" data-id="message id">
                       <h3>Title</h3>
                       <span>Author | Date</span>
                   </div>
                </li>
            </ul>
        </div>
        <div class="panel-footer">
            <ul>
                <li><a href="#">View Post</a></li>
                <li><a href="#">Delete</a></li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default" id="contact-message-info">
        <div class="panel-heading">
            <button id="modal-close">Close</button>
        </div>
        <div class="panel-body"></div>
    </div>
     -->
    
    @if($posts->lastPage() > 1)
        <ul class="pagination">
        	{{ $posts->links() }}
        </ul>
    @endif
@endsection

@section('scripts')

    <script type="text/javascript">
        var token = "{{ Session::token() }}";
    </script>
    <script type="text/javascript" src="{{ URL::secure('src/js/modal.js') }}"></script>
    <script type="text/javascript" src="{{ URL::secure('src/js/contact-message.js') }}"></script>

@endsection