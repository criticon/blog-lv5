@extends('layouts.admin-master')

@section('content')

    @include('includes.info-box')
    <form action="{{ route('admin.blog.post.create') }}" method="post">
        <div class="form-group">
            <label for="title">Title *</label>
            <input type="text" id="title" name="title" placeholder="Title" value="{{ Request::old('title') }}" class="form-control" />
        </div>
        <div class="form-group">
            <label for="author">Author *</label>
            <input type="text" id="author" name="author" placeholder="Author" value="{{ Request::old('author') }}" class="form-control" />
        </div>
        <div class="form-group">
            <label for="category_select">Add categories *</label>
            <select name="category_select" id="category_select">
                <option value="dummy category id">Dummy category</option>
            </select>
        </div>
        <button type="button" class="btn">Add category</button>
        <div>
            <!-- added categories -->
            <ul></ul>
        </div>
        <input type="hidden" name="categories" id="categories" />
        <div class="form-group">
            <label for="body">Body *</label>
            <textarea name="body" id="body" rows="10" placeholder="Body" class="form-control">{{ Request::old('body') }}</textarea>
        </div>
        <button type="submit" class="btn">Create post</button>
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
    </form>

@endsection

@section('scripts')

    <script type="text/javascript" src="{{ URL::secure('src/js/posts.js') }}"></script>

@endsection