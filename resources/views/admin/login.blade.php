@extends('layouts.master')

@section('title', 'Login')

@section('content')

@if(Session::has('fail'))
    <div>{{ Session::get('fail') }}</div>
@endif

@include('includes.info-box')

<form action="{{ route('admin.login') }}" method="post">
	<div class="form-group">
		<label for="email">Your Name</label>
		<input type="text" name="email" id="email" placeholder="Your email" value="{{ Request::old('email') }}" class="form-control" />
	</div>
	<div class="form-group">
		<label for="password">Your Password</label>
		<input type="password" name="password" id="password" placeholder="Your password" class="form-control" />
	</div>
	<button type="submit" class="btn">Submit</button>
	<input type="hidden" name="_token" value="{{ Session::token() }}" />
</form>

@endsection
