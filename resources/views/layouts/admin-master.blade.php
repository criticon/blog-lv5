<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin area</title>
        <link rel="stylesheet" href="{{ URL::to('src/css/bootstrap.min.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="{{ URL::to('src/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        @yield('styles')
    </head>
    <body>
        @include('includes.admin-header')
        
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    @yield('content')
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            var baseUrl = "{{ URL::to('/') }}";
        </script>
        @yield('scripts')
    </body>
</html>